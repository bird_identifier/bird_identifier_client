function clearAuthentication() {
    localStorage.removeItem(AUTH_TOKEN);
    localStorage.removeItem(AUTH_USERNAME);
    localStorage.removeItem(AUTH_TIME);

}

function storeAuthentication(session) {
    // remember me ON
    localStorage.setItem(AUTH_TOKEN, session.token);
    localStorage.setItem(AUTH_USERNAME, session.username);
    localStorage.setItem(AUTH_TIME, parseInt(new Date().getTime() / 1000));


    //sessionStorage.setItem(AUTH_TOKEN, session.token); remember me OFF
}

function getUsername() {
    return localStorage.getItem(AUTH_USERNAME);
}

function getToken() {
    return localStorage.getItem(AUTH_TOKEN);
}

function getAuthTime() {
    return localStorage.getItem(AUTH_TIME);
}

