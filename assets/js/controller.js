// Global user greeter
function greetUserOnLogin() {
    let greetUser = document.getElementById('greetUser').innerHTML = "Howdy, " + getUsername() + "!";
}

function dontDisplayGreeting() {
    document.getElementById('greetUser').innerHTML = '';
}


// --- Do I have localStorage.getItem('VALIIT_AUTH_TOKEN') ---
function getAuthToken() {
    let addBirdButton = document.getElementById("addBirdButton");
    let removeCol = document.getElementById("removeCol");
    let editCol = document.getElementById("editCol");
    let login = document.getElementById("login");
    let logout = document.getElementById("logout");


    if (getToken() != null) {
        login.style.display = "none";
        logout.style.display = "block";
        addBirdButton.style.display = "block";
        removeCol.style.display = "table-cell";
        editCol.style.display = "table-cell";
        greetUserOnLogin();
        sessionTimer();
        
    } else {
        logout.style.display = "none";
        login.style.display = "block";
        addBirdButton.style.display = "none";
        removeCol.style.display = "none";
        editCol.style.display = "none";
        dontDisplayGreeting();
        stopSessionTimer();

    }
}

// just use enter........
$('#loginForm').submit(function (event) {
    // prevent default browser behaviour
    event.preventDefault();

    // check user input
    if (!validateLoginInput()) {
        return;
    }

    loginUser();
});

let saveCredentials = null;

// handle user login
function loginUser() {
    let credentials = null;
     if (saveCredentials == null) {
        credentials = getCredentialsFromLoginContainer();
         if (document.getElementById("rememberMe").checked) {
             saveCredentials = credentials;
         }
     } else {
        
        credentials = getCredentialsFromLoginContainer();

     }
    
    // Kui checkbox on clickitud siis salvest credentialid ja kui järgmine kord kutsutakse sisesta ise kredentialid
    
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            loadBirds();
            getAuthToken();
            closeLoginModal();
        })
    }
}

// handle login error
function displayError(errorMessage) {
    let errorBox = document.querySelector('#loginError');
    errorBox.style.display = 'block';
    errorBox.innerText = errorMessage;
}

function hideError() {
    let errorBox = document.querySelector('#loginError');
    errorBox.style.display = 'none';
    errorBox.innerText = '';
}

// Validate user login input
function validateLoginInput() {
    let username = document.querySelector('#username').value;
    let password = document.querySelector('#password').value;

    if(username == null || username.length < 2) {
        displayError('Username missing!');
        return false;
    }

    if(password == null || password.length < 2) {
        displayError('Password missing!');
        return false;
    }
    return true;
}

// Handle logout user
function logoutUser() {
    clearAuthentication();
    cleanLoginForm();
    getAuthToken();
    loadBirds();
    hideError();
    document.getElementById("tokenTimeoutWarning").style.display = "none";
}

function handleBirdAddModalOpening() {
    cleanBirdForms();
    $('#birdModal').modal('show');
}


async function handleBirdEditModalOpening(id) {
    cleanBirdForms();

    let bird = await getBird(id);
    if (bird) {
        $('#birdModal').modal('show');
        document.querySelector('#birdId').value = bird.id;
        document.querySelector('#photo').value = bird.photo;
        document.querySelector('#name').value = bird.name;
        document.querySelector('#sound').value = bird.soundURL;
        document.querySelector('#flightMode').value = bird.flightMode;
        document.querySelector('#flightPattern').value = bird.flightPattern;
        document.querySelector('#basicAddColor').value = bird.basicColor;
        document.querySelector('#headAddColor').value  = bird.headColor;
        document.querySelector('#headColorCheek').value  = bird.headColorCheek;
        document.querySelector('#headColorForehead').value  = bird.headColorForehead;
        document.querySelector('#headColorBack').value  = bird.headColorBack;
        document.querySelector('#headColorNeck').value  = bird.headColorNeck;
        document.querySelector('#wingsAddColor').value  = bird.wingsColor;
        document.querySelector('#wingsColorAbove').value  = bird.wingsColorAbove;
        document.querySelector('#wingsColorBelow').value  = bird.wingsColorBelow;
        document.querySelector('#wingsColorPattern').value  = bird.wingsColorPattern;
        document.querySelector('#tailColor').value  = bird.tailColor;
        document.querySelector('#tailColorAbove').value  = bird.tailColorAbove;
        document.querySelector('#tailColorBelow').value  = bird.tailColorBelow;
        document.querySelector('#tailColorPattern').value  = bird.tailColorPattern;
        document.querySelector('#chestAddColor').value  = bird.chestColor;
        document.querySelector('#chestColorPattern').value  = bird.chestColorPattern;

        // korda üle..
        let birdLocationsSelect = document.querySelector('#locations');
        for(let i = 0; i < birdLocationsSelect.options.length; i++) {
            birdLocationsSelect.options[i].selected = bird.locations.some(birdLocation => birdLocation === birdLocationsSelect.options[i].value);
        }
    }
}



// Handle bird deletion
async function handleBirdDelete() {
    await deleteBird(document.querySelector("#birdIdToDelete").value);
    $('#deleteContainer').modal('hide');
    loadBirds();   
}

//Handle delete bird modal
function handleBirdDeleteModal(birdId) {
    
    $('#deleteContainer').modal('show');
    document.querySelector("#birdIdToDelete").value = birdId;
}

// Handle login modal
function handleLoginModalOpening() {
    $('#loginContainer').modal('show');
    if (saveCredentials != null) {
        savedCredential(saveCredentials);
    }
    
}

function closeLoginModal() {
    $("#loginContainer").modal('hide');
}

async function handleSave() {

    if (!validateBirdNameInput()) {
        return;
    }

    if (parseInt(document.querySelector('#birdId').value) > 0) {
        //update
        let bird = {
            id: document.querySelector('#birdId').value,
            photo: document.querySelector('#photo').value,
            name: document.querySelector('#name').value,
            soundURL: document.querySelector('#sound').value,
            flightMode: document.querySelector('#flightMode').value,
            flightPattern: document.querySelector('#flightPattern').value,
            basicColor: document.querySelector('#basicAddColor').value,
            headColor: document.querySelector('#headAddColor').value,
            headColorCheek: document.querySelector('#headColorCheek').value,
            headColorForehead: document.querySelector('#headColorForehead').value,
            headColorBack: document.querySelector('#headColorBack').value,
            headColorNeck: document.querySelector('#headColorNeck').value,
            wingsColor: document.querySelector('#wingsAddColor').value,
            wingsColorAbove: document.querySelector('#wingsColorAbove').value,
            wingsColorBelow: document.querySelector('#wingsColorBelow').value,
            wingsColorPattern: document.querySelector('#wingsColorPattern').value,
            tailColor: document.querySelector('#tailColor').value,
            tailColorAbove: document.querySelector('#tailColorAbove').value,
            tailColorBelow: document.querySelector('#tailColorBelow').value,
            tailColorPattern: document.querySelector('#tailColorPattern').value,
            chestColor: document.querySelector('#chestAddColor').value,
            chestColorPattern: document.querySelector('#chestColorPattern').value,
            locations: [...document.querySelector('#locations').options].filter(o => o.selected).map(o => o.value)
        };
        await editBird(bird);
    
    } else {
        //add
        let bird = {

            photo: document.querySelector('#photo').value,
            name: document.querySelector('#name').value,
            soundURL: document.querySelector('#sound').value,
            flightMode: document.querySelector('#flightMode').value,
            flightPattern: document.querySelector('#flightPattern').value,
            basicColor: document.querySelector('#basicAddColor').value,
            headColor: document.querySelector('#headAddColor').value,
            headColorCheek: document.querySelector('#headColorCheek').value,
            headColorForehead: document.querySelector('#headColorForehead').value,
            headColorBack: document.querySelector('#headColorBack').value,
            headColorNeck: document.querySelector('#headColorNeck').value,
            wingsColor: document.querySelector('#wingsAddColor').value,
            wingsColorAbove: document.querySelector('#wingsColorAbove').value,
            wingsColorBelow: document.querySelector('#wingsColorBelow').value,
            wingsColorPattern: document.querySelector('#wingsColorPattern').value,
            tailColor: document.querySelector('#tailColor').value,
            tailColorAbove: document.querySelector('#tailColorAbove').value,
            tailColorBelow: document.querySelector('#tailColorBelow').value,
            tailColorPattern: document.querySelector('#tailColorPattern').value,
            chestColor: document.querySelector('#chestAddColor').value,
            chestColorPattern: document.querySelector('#chestColorPattern').value,
            locations: [...document.querySelector('#locations').options].filter(o => o.selected).map(o => o.value)
        };
        await addBird(bird);
    }


    

    $('#birdModal').modal('hide');
    loadBirds();
}

// handle info Bird modal
function openInfoBirdModal(birdId) {
    $('#infoBirdModal').modal('show');


    for (let i = 0; i < birds.length; i++) {
        if(birds[i].id === birdId) {
            document.getElementById('modal-title').innerHTML = birds[i].name;
            document.getElementById('birdImg').innerHTML = `<img src="${birds[i].photo}" class="thumbnail" alt="photo" height="250">`
            document.getElementById('birdSong').innerHTML = `<audio controls><source src="${birds[i].soundURL}" type="audio/mpeg"></audio>` 
            document.getElementById('birdName').innerHTML = birds[i].desc;
            document.getElementById('wiki-link').innerHTML = `<a href="https://en.wikipedia.org/wiki/${birds[i].name}" target="_blank" class="btn btn-info" role="button">Read more..</a>`
        }
    }

}

// show more birds
function showAllBirds() {
    if (displayFirstBirdsOnly == false) {
        displayFirstBirdsOnly = true;
        document.getElementById("loadMoreButton").innerHTML = "Show all..";
    } else if (displayFirstBirdsOnly == true) {
        displayFirstBirdsOnly = false;
        document.getElementById("loadMoreButton").innerHTML = "Show less..";
    }
    

    loadBirds();
    
}

// Validate add bird name input
function validateBirdNameInput() {
    let birdName = document.querySelector('#name').value;
    let photo = document.querySelector('#photo').value; 

    if(birdName == null || birdName.length < 2) {
        displayInputError('Bird name missing!');
        return false;
    }
    if (photo == null || photo.length < 1) {
        displayInputError('Photo is missing!');
            return false;
        }
    
    if (!photo.startsWith("http://") && !photo.startsWith("https://")) {
        displayInputError('Photo URL is wrong. It must start with http:// or https://');
        return false;
    
    }
  

    
    return true;
}

// handle birdName error
function displayInputError(errorMessage) {
    let errorBox = document.querySelector('#inputError');
    errorBox.style.display = 'block';
    errorBox.innerText = errorMessage;
}
function hide3Error() {
    let errorBox = document.querySelector('#inputError');
    errorBox.style.display = 'none';
    errorBox.innerText = '';
}

// clean forms
function cleanBirdForms() {
    document.querySelector('#birdId').value = '';
    document.querySelector('#photo').value = '';
    document.querySelector('#name').value = '';
    document.querySelector('#sound').value = '';
    document.getElementById("file").value = '';
    document.querySelector('#flightMode').value = '';
    document.querySelector('#flightPattern').value = '';
    document.querySelector('#basicColor').value = '';
    document.querySelector('#headColor').value = '';
    document.querySelector('#headColorCheek').value = '';
    document.querySelector('#headColorForehead').value = '';
    document.querySelector('#headColorBack').value = '';
    document.querySelector('#headColorNeck').value = '';
    document.querySelector('#wingsColor').value = '';
    document.querySelector('#wingsColorAbove').value = '';
    document.querySelector('#wingsColorBelow').value = '';
    document.querySelector('#wingsColorPattern').value = '';
    document.querySelector('#tailColor').value = '';
    document.querySelector('#tailColorAbove').value = '';
    document.querySelector('#tailColorBelow').value = '';
    document.querySelector('#tailColorPattern').value = '';
    document.querySelector('#chestColor').value = '';
    document.querySelector('#chestColorPattern').value = '';
    document.querySelector('#locations').value = '';

    hideError();

}

//upload function
function saveFile(event) {
    event.preventDefault();
    let fileInput = getFileInput();
    uploadFile(fileInput.files[0]).then(response => fillSoundField(response.url));
}


