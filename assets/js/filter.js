
let birds = [];
let filteredBirds = [];
let colors = null;

let displayFirstBirdsOnly = true;

async function loadBirds() {
    if (!colors) {
        colors = await getColors();
    }

    birds = await getBirds();
    filteredBirds = birds;
    
    displayBirds(filteredBirds);
    console.log(filteredBirds)
}


// --- handle displaying birds ---
function displayBirds(birdsToDisplay) {
    let birdsHtml = '';

    let displayCount = birdsToDisplay.length;
    if (displayCount > 6 && displayFirstBirdsOnly){
        displayCount = 6;
    }

    if (getToken() != null) {
        for (let i = 0; i < displayCount; i++) {
            birdsHtml = birdsHtml + `
            <tr>
                <td><a href="#" onclick="openInfoBirdModal(${birdsToDisplay[i].id})">${birdsToDisplay[i].name}</a></td>
                <td><img src="${birdsToDisplay[i].photo}" class="thumbnail" alt="photo" height="50" ></td>
                <td><button type="button" class="btn btn-outline-warning" onclick="handleBirdEditModalOpening(${birdsToDisplay[i].id}), fillAddColorSelect()">Edit</button></td>
                <td><button type="button" class="btn btn-outline-danger" onclick="handleBirdDeleteModal(${birdsToDisplay[i].id})">Delete</button></td>
            </tr>
            `;
        }
    } else {
        for (let i = 0; i < displayCount; i++) {
            birdsHtml = birdsHtml + `
            <tr>
                <td><a href="#" onclick="openInfoBirdModal(${birdsToDisplay[i].id})">${birdsToDisplay[i].name}</a></td>
                <td><img src="${birdsToDisplay[i].photo}" class="thumbnail" alt="photo" height="50" ></td>
            </tr>
            `;
        }
    }


    document.querySelector('#birdsList').innerHTML = birdsHtml;    
}

// -- handle filter change that goes over all selected options ---
function handleFilterChange() {
    handleLocationFilterChange();
    handleColorFilterChange();
    handleHeadColorFilterChange();
    handleChestColorFilterChange();
    handleWingsColorFilterChange();
    displayBirds(filteredBirds);
    

}

// --- Location filter ---
function handleLocationFilterChange() {
    let selectedLocations = Array.from(document.querySelectorAll('input[name=location]:checked')).map(e => e.value);
    filteredBirds = birds.filter(b => doesBordhaveLocation(b, selectedLocations));
}

// -- Location filter assist function
function doesBordhaveLocation(bird, selectedLocations) {
    if(selectedLocations.length == 0) {
        return true;
    } else {
        return selectedLocations.find(l => bird.locations.findIndex(bl => bl == l) > -1);
    }
}

function fillColorFilters() {
    fillColorSelect();
    fillHeadColorSelect();
    fillChestColorSelect();
    fillWingsColorSelect();
}

// -- handle basic color dropdown fill ---
function fillColorSelect() {    
    document.querySelector('#basicColor').options.length = 0;
    let colorOption = document.createElement('option');
    colorOption.text = "";
    colorOption.value = "";
    document.querySelector('#basicColor').add(colorOption);

    for(let i = 0; i < colors.length; i++) {
        if (filteredBirds.findIndex(b => b.basicColor == colors[i].key) > -1) {
            let colorOption = document.createElement('option');
            colorOption.text = colors[i].text;
            colorOption.value = colors[i].key;
            document.querySelector('#basicColor').add(colorOption);
        }
    }
}

// -- handle head color dropdown fill ---
function fillHeadColorSelect() {    
    document.querySelector('#headColor').options.length = 0;
    let colorOption = document.createElement('option');
    colorOption.text = "";
    colorOption.value = "";
    document.querySelector('#headColor').add(colorOption);

    for(let i = 0; i < colors.length; i++) {
        if (filteredBirds.findIndex(b => b.headColor == colors[i].key) > -1) {
            let colorOption = document.createElement('option');
            colorOption.text = colors[i].text;
            colorOption.value = colors[i].key;
            document.querySelector('#headColor').add(colorOption);
        }
    }
}
//--- handle chest color dropdown fill ---
function fillChestColorSelect() {    
    document.querySelector('#chestColor').options.length = 0;
    let colorOption = document.createElement('option');
    colorOption.text = "";
    colorOption.value = "";
    document.querySelector('#chestColor').add(colorOption);

    for(let i = 0; i < colors.length; i++) {
        if (filteredBirds.findIndex(b => b.chestColor == colors[i].key) > -1) {
            let colorOption = document.createElement('option');
            colorOption.text = colors[i].text;
            colorOption.value = colors[i].key;
            document.querySelector('#chestColor').add(colorOption);
        }
    }
}
//--- handle wings color dropdown fill ---
function fillWingsColorSelect() {    
    document.querySelector('#wingsColor').options.length = 0;
    let colorOption = document.createElement('option');
    colorOption.text = "";
    colorOption.value = "";
    document.querySelector('#wingsColor').add(colorOption);

    for(let i = 0; i < colors.length; i++) {
        if (filteredBirds.findIndex(b => b.wingsColor == colors[i].key) > -1) {
            let colorOption = document.createElement('option');
            colorOption.text = colors[i].text;
            colorOption.value = colors[i].key;
            document.querySelector('#wingsColor').add(colorOption);
        }
    }
}

// --- Color filter ---
function handleColorFilterChange() {
    let selectedColor = document.querySelector('#basicColor').value;
    filteredBirds = filteredBirds.filter(b => selectedColor === "" || b.basicColor === selectedColor);
}

// --- Head Color filter ---
function handleHeadColorFilterChange() {
    let selectedColor = document.querySelector('#headColor').value;
    filteredBirds = filteredBirds.filter(b => selectedColor === "" || b.headColor === selectedColor);
}

// --- Chest Color filter ---
function handleChestColorFilterChange() {
    let selectedColor = document.querySelector('#chestColor').value;
    filteredBirds = filteredBirds.filter(b => selectedColor === "" || b.chestColor === selectedColor);
}

// --- Wings Color filter ---
function handleWingsColorFilterChange() {
    let selectedColor = document.querySelector('#wingsColor').value;
    filteredBirds = filteredBirds.filter(b => selectedColor === "" || b.wingsColor === selectedColor);
}
