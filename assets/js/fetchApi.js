
// Functions for communicating with backend-API here...
async function getBirds() {
    // alustab
    document.getElementById("spinner").style.display = "block";
    document.getElementById("showAll").style.display = "none";

    let result = await fetch(`${API_URL}/birds`);

    // lõpetab
    document.getElementById("spinner").style.display = "none";
    document.getElementById("showAll").style.display = "block";
    
    let birds = await result.json();
    //console.log(birds);
    return birds;
}

// get bird id 
async function getBird(id) {
    let result = await fetch(`${API_URL}/birds/${id}`);
    let bird = await result.json();
    //console.log(bird);
    return bird;
}

async function editBird(bird) {
    await fetch(`${API_URL}/birds`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(bird)
    });
}

//Bird delete
async function deleteBird(id) {
    await fetch(`${API_URL}/birds/${id}`, {
        method: 'Delete',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    });
}

async function addBird(bird) {
    await fetch(`${API_URL}/birds`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(bird)
    });
}

async function extend() {
    let response = await fetch(
        `${API_URL}/users/extend`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${getToken()}`,
                'Content-Type': 'application/json'
            },
        }
    );
    checkResponse(response);
    let session = await response.json();
    storeAuthentication(session);
    //console.log(credentials);
}

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(session => session.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        //throw a error message for user 
        displayError('Username or Password is wrong!');
        throw new Error(response.status);
    }
    //showMainContainer();
    return response;
}

// --- Get all colors ---
async function getColors() {
  
    let result = await fetch(`${API_URL}/colors`);
    let colors = await result.json();  

    console.log(colors);
    return colors;


}

function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
            `${API_URL}/files/uploads`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse).then(response => response.json());
}