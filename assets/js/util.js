
// General helper functions here...
function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

// clear login form
function cleanLoginForm() {
    document.getElementById("username").value = '';
    document.getElementById("password").value = '';
}

// for remember me checkbox - do we keep it?
function savedCredential(credentials) {
    document.getElementById("username").value = credentials.username;
    document.getElementById("password").value = credentials.password;
}

function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

// Token timeout session interval
var sessionInterval; //var - because of access
function sessionTimer() {
    if (sessionInterval == null) {
        sessionInterval = setInterval(function () {

            if ((parseInt(new Date().getTime() / 1000) - getAuthTime()) > 480) { // 8min. saada häire
                //display warning
                document.getElementById("tokenTimeoutWarning").style.display = "block";

                if ((parseInt(new Date().getTime() / 1000) - getAuthTime()) > 580) { // 9,3min. lõpeta sessioon
                    //end session
                    logoutUser();
                }

            }

        }, 10000);

    }
}

function stopSessionTimer() {
    clearInterval(sessionInterval);
    sessionInterval = null;
}

// Token timeout session extend
let continueSessionBtn = document.querySelector('#continueSession');

continueSessionBtn.addEventListener('click', function () {
    extend();
    document.getElementById("tokenTimeoutWarning").style.display = "none";
});

// Add bird - fill color options
addColorToSelect({ key: "", text: "" }, document.querySelector('#basicAddColor'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#headAddColor'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#wingsAddColor'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#chestAddColor'));
// -- aditional --
addColorToSelect({ key: "", text: "" }, document.querySelector('#headColorCheek'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#headColorForehead'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#headColorBack'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#headColorNeck'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#wingsColorAbove'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#wingsColorBelow'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#tailColor'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#tailColorAbove'));
addColorToSelect({ key: "", text: "" }, document.querySelector('#tailColorBelow'));

// Populate colors
function fillAddColorSelect() {
    for (let i = 0; i < colors.length; i++) {
        addColorToSelect(colors[i], document.querySelector('#basicAddColor'));
        addColorToSelect(colors[i], document.querySelector('#headAddColor'));
        addColorToSelect(colors[i], document.querySelector('#wingsAddColor'));
        addColorToSelect(colors[i], document.querySelector('#chestAddColor'));
        // -- aditional --
        addColorToSelect(colors[i], document.querySelector('#headColorCheek'));
        addColorToSelect(colors[i], document.querySelector('#headColorForehead'));
        addColorToSelect(colors[i], document.querySelector('#headColorBack'));
        addColorToSelect(colors[i], document.querySelector('#headColorNeck'));
        addColorToSelect(colors[i], document.querySelector('#wingsColorAbove'));
        addColorToSelect(colors[i], document.querySelector('#wingsColorBelow'));
        addColorToSelect(colors[i], document.querySelector('#tailColor'));
        addColorToSelect(colors[i], document.querySelector('#tailColorAbove'));
        addColorToSelect(colors[i], document.querySelector('#tailColorBelow'));
    }
}

function addColorToSelect(color, selectComponent) {
    let colorOption = document.createElement('option');
    colorOption.text = color.text;
    colorOption.value = color.key;
    selectComponent.add(colorOption);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

//fill sound
function fillSoundField(sound) {
    document.getElementById("sound").value = sound;
}


//   function play(){
//        let audio = document.getElementById("audio");
//        audio.play(); 
//     }
                 
